package tables;

import com.atlassian.pocketknife.spi.querydsl.EnhancedRelationalPathBase;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;

public class QEmployee extends EnhancedRelationalPathBase<QEmployee> {

    public NumberPath<Long> ID = createLongCol("ID").asPrimaryKey().build();
    public StringPath NAME = createStringCol("NAME").build();
    public NumberPath<Long> MANAGER_ID = createLongCol("MANAGER_ID").build();

    public QEmployee() {
        super(QEmployee.class, "EMPLOYEE");
    }

    public QEmployee(String alias) {
        super(QEmployee.class, "EMPLOYEE", alias);
    }
}
