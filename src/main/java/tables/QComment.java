package tables;

import com.atlassian.pocketknife.spi.querydsl.EnhancedRelationalPathBase;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;
import model.CommentDTO;


public class QComment extends EnhancedRelationalPathBase<CommentDTO> {

    public NumberPath<Long> ID = createLongCol("ID").asPrimaryKey().build();
    public NumberPath<Long> POST_ID = createLongCol("POST_ID").build();
    public StringPath CONTENT = createStringCol("CONTENT").build();

    public QComment() {
        super(CommentDTO.class, "COMMENT");
    }
}
