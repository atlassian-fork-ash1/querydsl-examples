package tables;

import com.atlassian.pocketknife.spi.querydsl.EnhancedRelationalPathBase;
import com.querydsl.core.types.dsl.NumberPath;

public class QProductItem extends EnhancedRelationalPathBase<QProductItem> {

    public NumberPath<Long> ID = createLongCol("ID").asPrimaryKey().build();
    public NumberPath<Long> PRODUCT_ID = createLongCol("PRODUCT_ID").build();
    public NumberPath<Long> SKU_ID = createLongCol("SKU_ID").build();
    public NumberPath<Long> PRICE = createLongCol("PRICE").build();

    public QProductItem() {
        super(QProductItem.class, "PRODUCT_ITEM");
    }
}
