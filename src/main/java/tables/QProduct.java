package tables;

import com.atlassian.pocketknife.spi.querydsl.EnhancedRelationalPathBase;
import com.querydsl.core.types.dsl.DatePath;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;
import model.ProductBean;

import java.util.Date;


public class QProduct extends EnhancedRelationalPathBase<ProductBean> {

    public NumberPath<Long> ID = createLongCol("ID").asPrimaryKey().build();
    public StringPath NAME = createStringCol("NAME").build();
    public DatePath<Date> LAUNCH_DATE = createDateCol("LAUNCH_DATE", Date.class).build();

    public QProduct() {
        super(ProductBean.class, "PRODUCT");
    }
}
