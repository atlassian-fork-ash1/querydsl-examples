package model;

import java.util.Date;

public class ProductDTO {
    private final Long id;
    private final String name;
    private final Date launchDate;

    public ProductDTO(Long id, String name, Date launchDate) {
        this.id = id;
        this.name = name;
        this.launchDate = launchDate;
    }

    public Long getId() {
        return id;
    }

    public Date getLaunchDate() {
        return launchDate;
    }

    public String getName() {
        return name;
    }
}
