package model;

import java.util.List;

public class PostDTO {
    private final Long id;
    private final String title;
    private final String content;
    private final List<CommentDTO> comments;

    public PostDTO(Long id, String title, String content, List<CommentDTO> comments) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.comments = comments;
    }

    public String getContent() {
        return content;
    }

    public Long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public List<CommentDTO> getComments() {
        return comments;
    }
}
