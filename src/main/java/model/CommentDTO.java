package model;

public class CommentDTO {
    private final Long id;
    private final String content;

    public CommentDTO(Long id, String content) {
        this.id = id;
        this.content = content;
    }

    public Long getId() {
        return id;
    }

    public String getContent() {
        return content;
    }
}
